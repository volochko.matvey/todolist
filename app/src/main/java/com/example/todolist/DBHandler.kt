package com.example.todolist

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.todolist.model.Todo
import com.example.todolist.model.TodoItem

class DBHandler(val context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        val createToDoTable = "  CREATE TABLE $TABLE_TODO (" +
                "$COL_ID integer PRIMARY KEY AUTOINCREMENT," +
                "$COL_CREATED_AT datetime DEFAULT CURRENT_TIMESTAMP," +
                "$COL_NAME varchar);"
        val createToDoItemTable =
            "CREATE TABLE $TABLE_TODO_ITEM (" +
                    "$COL_ID integer PRIMARY KEY AUTOINCREMENT," +
                    "$COL_CREATED_AT datetime DEFAULT CURRENT_TIMESTAMP," +
                    "$COL_TODO_ID integer," +
                    "$COL_ITEM_NAME varchar," +
                    "$COL_IS_COLPLETED integer);"

        db.execSQL(createToDoTable)
        db.execSQL(createToDoItemTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun addTodo(todo: Todo): Boolean {
        val db = writableDatabase
        val contentValue = ContentValues()
        contentValue.put(COL_NAME, todo.name)
        val result = db.insert(TABLE_TODO, null, contentValue)
        return result != (-1).toLong()
    }

    fun updateTodo(todo: Todo) {
        val db = writableDatabase
        val contentValue = ContentValues()
        contentValue.put(COL_NAME, todo.name)
        db.update(TABLE_TODO, contentValue, "$COL_ID=?", arrayOf(todo.id.toString()))
    }

    fun getTodoList(): MutableList<Todo> {
        val result: MutableList<Todo> = ArrayList()
        val db = readableDatabase
        val queryResult = db.rawQuery("SELECT * from $TABLE_TODO", null)
        if (queryResult.moveToFirst()) {
            do {
                val todo = Todo()
                todo.id = queryResult.getLong(queryResult.getColumnIndex(COL_ID))
                todo.name = queryResult.getString(queryResult.getColumnIndex(COL_NAME))
                result.add(todo)
            } while (queryResult.moveToNext())
        }
        queryResult.close()
        return result
    }

    fun deleteToDoItem(itemId : Long){
        val db = writableDatabase
        db.delete(TABLE_TODO_ITEM,"$COL_ID=?" , arrayOf(itemId.toString()))
    }

    fun addTodoItem(todoItem: TodoItem): Boolean {
        val db = writableDatabase
        val contentValue = ContentValues()
        contentValue.put(COL_ITEM_NAME, todoItem.itemName)
        contentValue.put(COL_TODO_ID, todoItem.id)
        if (todoItem.isCompleted) {
            contentValue.put(COL_IS_COLPLETED, true)
        } else {
            contentValue.put(COL_IS_COLPLETED, false)
        }
        val result = db.insert(TABLE_TODO_ITEM, null, contentValue)
        return result != (-1).toLong()
    }

    fun updateTodoItem(todoItem: TodoItem) {
        val db = writableDatabase
        val cv = ContentValues()
        cv.put(COL_ITEM_NAME, todoItem.itemName)
        cv.put(COL_TODO_ID, todoItem.todoId)
        cv.put(COL_IS_COLPLETED, todoItem.isCompleted)

        db.update(TABLE_TODO_ITEM, cv, "$COL_ID=?", arrayOf(todoItem.id.toString()))

    }

    fun deleteTodo(todoId: Long) {
        val db = readableDatabase
        db.delete(TABLE_TODO, "$COL_ID=?", arrayOf(todoId.toString()))
        db.delete(TABLE_TODO_ITEM, "$COL_TODO_ID=?", arrayOf(todoId.toString()))
    }

    fun getTodoItems(todoId: Long): MutableList<TodoItem> {
        val result: MutableList<TodoItem> = ArrayList()
        val db = readableDatabase
        val queryResult = db.rawQuery("SELECT * FROM $TABLE_TODO_ITEM WHERE $COL_TODO_ID = $todoId", null)
        if (queryResult.moveToFirst()) {
            do {
                val todoItem = TodoItem()
                todoItem.id = queryResult.getLong(queryResult.getColumnIndex(COL_ID))
                todoItem.todoId = queryResult.getLong(queryResult.getColumnIndex(COL_TODO_ID))
                todoItem.itemName = queryResult.getString(queryResult.getColumnIndex(COL_ITEM_NAME))
                todoItem.isCompleted = queryResult.getInt(queryResult.getColumnIndex(COL_IS_COLPLETED)) == 1
                result.add(todoItem)
            } while (queryResult.moveToNext())
        }


        queryResult.close()
        return result
    }
}