package com.example.todolist.model

class Todo {

    var id : Long = -1
    var name : String = ""
    var createId = ""
    var items : MutableList<TodoItem > = ArrayList()

}