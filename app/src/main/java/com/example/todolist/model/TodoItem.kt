package com.example.todolist.model

class TodoItem {

    var id : Long = -1
    var todoId :Long = -1
    var itemName = ""
    var isCompleted = false
}