package com.example.todolist

import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.todolist.model.TodoItem
import kotlinx.android.synthetic.main.activity_item.*

class ItemActivity : AppCompatActivity() {

    lateinit var dbHandler: DBHandler
    var todoid: Long = -1
    var todoName: String = ""
    lateinit var view: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.title = intent.getStringExtra(INTENT_TODO_NAME)
        rv_item.layoutManager = LinearLayoutManager(this)
        todoid = intent.getLongExtra(COL_TODO_ID, -1)
        dbHandler = DBHandler(this)

        fab_item.setOnClickListener {
            val dialog = AlertDialog.Builder(this)
            view = layoutInflater.inflate(R.layout.dialog_main, null)
            val toDoName = view.findViewById<EditText>(R.id.ev_todo)
            dialog.setView(view)
            dialog.setPositiveButton("Add") { _: DialogInterface, _: Int ->
                if (toDoName.text.isNotEmpty()) {
                    val toDo = TodoItem()
                    toDo.itemName = toDoName.text.toString()
                    toDo.id = todoid
                    toDo.isCompleted = false
                    dbHandler.addTodoItem(toDo)
                    refreshList()
                }
            }
            dialog.setNegativeButton("Cancel") { _: DialogInterface, _: Int ->

            }
            dialog.show()
        }
    }

    override fun onResume() {
        super.onResume()
        refreshList()

    }

    fun updateItem(item: TodoItem) {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Update ToDo Item")
        val view = layoutInflater.inflate(R.layout.dialog_main, null)
        val toDoName = view.findViewById<EditText>(R.id.ev_todo)
        toDoName.setText(item.itemName)
        dialog.setView(view)
        dialog.setPositiveButton("Update") { _: DialogInterface, _: Int ->
            if (toDoName.text.isNotEmpty()) {
                item.itemName = toDoName.text.toString()
                item.todoId = todoid
                item.isCompleted = false
                dbHandler.updateTodoItem(item)
                refreshList()
            }
        }
        dialog.setNegativeButton("Cancel") { _: DialogInterface, _: Int ->

        }
        dialog.show()
    }

    private fun refreshList() {
        rv_item.adapter = ItemAdapter(this, dbHandler.getTodoItems(todoid))
    }

    class ItemAdapter(val activity: ItemActivity, val list: MutableList<TodoItem>) :
            RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
        private var newPosition: Int = -1

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.rv_child_item, parent, false))
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            newPosition = holder.adapterPosition
            holder.tv_text.text = list[position].itemName
//            holder.tv_text.setOnClickListener {
//                list[position].isCompleted = !list[position].isCompleted
            activity.dbHandler.updateTodoItem(list[position])


//            }
            holder.delete.setOnClickListener {
                activity.dbHandler.deleteToDoItem(list[position].id)
                activity.refreshList()
            }
            holder.refactor.setOnClickListener {
                activity.updateItem(list[position])
            }
        }

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_text = view.findViewById<TextView>(R.id.cb_item)
            val refactor = view.findViewById<ImageView>(R.id.iv_menu_refactor)
            val delete = view.findViewById<ImageView>(R.id.iv_menu_delete)

        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }

    }
}