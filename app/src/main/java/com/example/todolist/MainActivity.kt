package com.example.todolist

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import com.example.todolist.model.Todo
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var dbHandler: DBHandler = DBHandler(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dbHandler = DBHandler(this)
        rv_main.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        setSupportActionBar(dashboard_toolbar)
        title = "TodoList"
        fab_main.setOnClickListener {
            val dialog = AlertDialog.Builder(this)
            val view = layoutInflater.inflate(R.layout.dialog_main, null)
            val toDoName = view.findViewById<EditText>(R.id.ev_todo)
            dialog.setView(view)
            dialog.setPositiveButton("Add") { _: DialogInterface, _: Int ->
                if (toDoName.text.isNotEmpty()) {
                    val toDo = Todo()
                    toDo.name = toDoName.text.toString()
                    dbHandler.addTodo(toDo)
                    refreshList()
                }
            }
            dialog.setNegativeButton("Cancel") { _: DialogInterface, _: Int ->

            }
            dialog.show()
        }

    }

    fun updateTodo(todo: Todo){
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Todo edit")
        val view = layoutInflater.inflate(R.layout.dialog_main, null)
        val toDoName = view.findViewById<EditText>(R.id.ev_todo)
        toDoName.setText(todo.name)
        dialog.setView(view)
        dialog.setPositiveButton("Update") { _: DialogInterface, _: Int ->
            if (toDoName.text.isNotEmpty()) {
                todo.name = toDoName.text.toString()
                dbHandler.updateTodo(todo)
                refreshList()
            }
        }
        dialog.setNegativeButton("Cancel") { _: DialogInterface, _: Int ->

        }
        dialog.show()
    }

    override fun onResume() {
        super.onResume()
        refreshList()

    }

    private fun refreshList() {
        rv_main.adapter = MainAdapter(this, dbHandler.getTodoList())
    }


    class MainAdapter(val activity: MainActivity, val list: MutableList<Todo>) :
        RecyclerView.Adapter<MainAdapter.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.rv_childmain, parent, false))
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.todoName.text = list[position].name

            holder.todoName.setOnClickListener {
                val intent = Intent(activity, ItemActivity::class.java)
                intent.putExtra(INTENT_TODO_ID, list[position].id)
                intent.putExtra(INTENT_TODO_NAME, list[position].name)
                activity.startActivity(intent)
            }

            holder.menu.setOnClickListener {
                val popup = PopupMenu(activity, holder.menu)
                popup.inflate(R.menu.menu_main)
                popup.setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.menu_delete -> {
                                activity.dbHandler.deleteTodo(list[position].id)
                                activity.refreshList()
                        }
                        R.id.menu_edit -> {
                            activity.updateTodo(list[position])
                        }
                    }
                    true
                }
                popup.show()
            }

        }

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val todoName = view.findViewById<TextView>(R.id.tv_todo_name)
            val menu = view.findViewById<ImageView>(R.id.iv_menu)
        }

    }


}
